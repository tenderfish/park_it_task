<?php 

namespace App\Http\Controllers;
 
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
class UserController extends Controller
{
    private $page_size = 10;
 
	public function createUser(Request $request)
	{
        $this->validate($request, [
            'forename' => 'required',
            'surname' => 'required',
            'email' => 'required|email'
        ]);

    	$user = User::create($request->all());
 
    	return response()->json($user);
 
	}
 
	public function updateUser(Request $request, $id)
	{  
        $this->validate($request, [
            'forename' => 'required',
            'surname' => 'required',
            'email' => 'required|email'
        ]);

        $user  = User::find($id);

        if(!$user) {
            return response()->json('Could not find user.');
        }
        
        $user->forename = $request->input('forename');
    	$user->surname = $request->input('surname');
    	$user->email = $request->input('email');

    	$user->save();
 
    	return response()->json($user);
	}  
 
	public function deleteUser($id){
    	$user  = User::find($id);

        if(!$user) {
            return response()->json('Could not find user.');
        }

    	$user->delete();
 
    	return response()->json('Removed successfully.');
	}
 
	public function index(){
 
    	$users  = User::paginate($this->page_size);
 
    	return response()->json($users);
 
	}
}
?>