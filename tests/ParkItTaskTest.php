<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class ParkItTaskTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Tests successful post to /users.
     *
     * @return void
     */
    public function testSuccessfulPost()
    {
        $this->json('POST', '/api/v1/users', ['forename' => 'Dan', 'surname' => 'Jelley', 'email' => 'danieljelley@hotmail.co.uk'])
            ->seeJson([
                'surname' => 'Jelley'
            ]);

    }

    /**
     * Tests unsuccessful posts to /users.
     *
     * @return void
     */
    public function testUnsuccessfulPosts()
    {
        $this->json('POST', '/api/v1/users', ['forename' => 'Dan', 'surname' => 'Jelley', 'email' => 'danieljelleyhotmail.co.uk'])
            ->seeJson([
                'email' => ['The email must be a valid email address.']
            ]);

        $this->json('POST', '/api/v1/users', ['forename' => '', 'surname' => 'Jelley', 'email' => 'danieljelleyhotmail.co.uk'])
            ->seeJson([
                'forename' => ["The forename field is required."]
            ]);

        $this->json('POST', '/api/v1/users', ['forename' => 'Dan', 'surname' => '', 'email' => 'danieljelleyhotmail.co.uk'])
            ->seeJson([
                'surname' => ["The surname field is required."]
            ]);

    }

    /**
     * Tests successful put to /users/{id}.
     *
     * @return void
     */
    public function testSuccessfulPut()
    {

        $user = factory('App\User')->make();
        $user->save();
        
        $this->json('PUT', '/api/v1/users/' . $user->id, ['forename' => 'Dan', 'surname' => 'Jelley', 'email' => 'danieljelley@hotmail.co.uk'])
            ->seeJson([
                'surname' => 'Jelley'
            ]);

    }

    /**
     * Tests successful delete to /users/{id}.
     *
     * @return void
     */
    public function testSuccessfulDelete()
    {

        $user = factory('App\User')->make();
        $user->save();
        
        $this->json('DELETE', '/api/v1/users/' . $user->id, [])
            ->seeJson([
                'Removed successfully.'
            ]);

    }
}
