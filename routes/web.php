<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'api/v1'], function($app)
{
	$app->post('users','UserController@createUser');
 
	$app->put('users/{id}','UserController@updateUser');
 	 
	$app->delete('users/{id}','UserController@deleteUser');
 
	$app->get('users','UserController@index');
});