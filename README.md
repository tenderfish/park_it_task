# Dan Jelley Park IT Technical Task Submission

### Task completed - API Creation :
https://github.com/holidayextras/culture/blob/master/recruitment/developer-API-task.md

### Solution repository :
https://bitbucket.org/tenderfish/park_it_task

### Solution working implementation :
http://parkit.danieljelley.com/api/v1/users

I have completed the API creation task using the 'Lumen' framework. I firmly believe in choosing the right tool for the right job, and for fast, lightweight APIs in PHP, Lumen is a great choice. Given the tight timescales (I've not had much time to work on this, for which I apologise!), I thought it best to choose a set of tools that would jump-start my solution, so I have used the following : 

* AWS hosting - fresh install on a t2.micro, had to configure LAMP stack & Composer
* Swagger UI for API documentation & testing
* Lumen framework
* Postman for local API testing
* Bitbucket repository

The solution implements 4 endpoints as described in Swagger UI documentation I have published here :
http://swagger.danieljelley.com

The endpoints are testable using the **'Try it out'** functionality from within Swagger UI; I have implemented a middleware in the API codebase to output the correct CORS headers to allow the cross-domain request through.

I have implemented some basic unit tests to verify each API endpoint works as intended.

I've also created a database seeder which currently adds 100 randomly generated users to the system for testing purposes.

#### Suggested Improvements :
* **HATEOAS functionality** : linking to the put / delete for each record when returning users. Currently links exist for 'next', 'prev' etc - these are output 'for free' by Lumen's pagination functionality (great framework).
* **Authentication** - all API requests are currently open - no authorisation exists.
